from csv import DictReader, DictWriter
import os


class Student:
    def __init__(self, assignment_types: list, id: str, lname: str, fname: str):
        self.id = id
        self.lname = lname
        self.fname = fname
        self.grades_dict = {assignment_type: []
                            for assignment_type in assignment_types}

    def add_assignment_type(self, assignment_type: str) -> None:
        if assignment_type not in self.grades_dict.keys():
            self.grades_dict[assignment_type] = []

    def list_assignment_types(self) -> list:
        return list(self.grades_dict.keys())

    def add_assignment(self, assignment_type: str, percentage: float) -> None:
        if assignment_type in self.grades_dict.keys():
            self.grades_dict[assignment_type].append(percentage)
        else:
            # TODO: Throw exception
            pass

    def type_average(self, assignment_type: str, drops: int = 0) -> float:
        if assignment_type in self.grades_dict.keys():
            # Sort grades from smallest to largest
            l = sorted(self.grades_dict[assignment_type])
            if len(l)-drops > 0:
                # l[drops:] is the list of grades sans the lowest drops-many
                return sum(l[drops:])/(len(l)-drops)
            else:
                # TODO: Throw exception
                return -1
        else:
            # TODO: Throw exception
            pass

    def average(self, type_weight_dict: dict) -> float:
        running_sum = 0.0

        # Include all types included in type_weight_dict
        # Types not listed in type_weight_dict are ignored (i.e. given weight 0)
        for t in self.list_assignment_types():
            if t in type_weight_dict.keys():
                running_sum = running_sum + type_weight_dict[t]*self.type_average(t)

        return running_sum

    def to_summary(self, type_weight_dict: dict, drops_dict: dict) -> dict:
        output = {'sid': self.id, 'lname': self.lname, 'fname': self.fname}

        for t in self.grades_dict.keys():
            drop = drops_dict[t] if t in drops_dict.keys() else 0
            output[t] = self.type_average(t, drops=drop)

        output['average'] = self.average(type_weight_dict)
        return output


def email_to_sid(email_addr: str) -> str:
    return email_addr.strip().split('@')[0]


def create_gradebook_from_gradescope_roster(csv_location: str, assignment_types: list) -> dict:
    gb = {}
    with open(csv_location) as csvfile:
        dictreader = DictReader(csvfile)
        for line in dictreader:
            sid = line['SID'].strip()
            lname = line['Last Name']
            fname = line['First Name']
            gb[sid] = Student(assignment_types, sid, lname, fname)
    return gb

# Parses the csv file at csv_location assuming it came from Gradescope
# gradebook - a dictionary mapping student ids to
# assignment_type - the type of the assignment, e.g. 'quiz' or 'pset'
def parse_gradescope_assignment(csv_location: str, gradebook: dict, assignment_type: str) -> None:
    with open(csv_location) as csvfile:
        dictreader = DictReader(csvfile)

        for line in dictreader:
            sid = line['SID'].strip()
            max_pts = float(line['Max Points'].strip())
            pts_earned = float(line['Total Score'].strip(
            )) if line['Total Score'].strip() != '' else 0.0
            gradebook[sid].add_assignment(assignment_type, pts_earned/max_pts)


def parse_gradescope_assignment_folder(dir: str, gradebook: dict, assignment_type: str) -> None:
    for f in os.listdir(dir):
        parse_gradescope_assignment(os.path.join(
            dir, f), gradebook, assignment_type)

# Parses Learning Catalytics csv gradebook for attendance
def parse_lc_attendance(csv_location: str, gradebook: dict, drops: float = 0) -> None:
    with open(csv_location) as csvfile:
        reader = DictReader(csvfile)

        # List of invalid field names (i.e. non-assignment fields)
        invalid = ['Last name', 'First name', 'Email', 'Student ID', 'Total Score']

        valid = []
        for field in reader.fieldnames:
            if field not in invalid:
                valid.append(field)
        num_lc = len(valid)

        for line in reader:

            # LC doesn't always have the student ids stored.
            # Students can choose to use a personal email here, so this isn't a very reliable
            # way of determing the SID. It works for me since, I edited the LC csv gradebook
            # with their official emails in them.
            sid = email_to_sid(line['Email'])
            count = 0
            # When a student logs into an LC session for a class, the corresponding
            # entry in the CSV is a number. When a student doesn't log in, that entry is blank.
            # This loop counts the number of classes attended by counting non-blank entries.
            for field in valid:
                if line[field].strip() != '':
                    count = count + 1
            gradebook[sid].add_assignment(
                'lc', min(float(count)/float(num_lc-drops), 1.0))

def parse_lc(csv_location: str, gradebook: dict, total_points: int, drops: float = 0, cap_percent: bool = True) -> None:
    with open(csv_location) as csvfile:
        reader = DictReader(csvfile)

        # List of invalid field names (i.e. non-assignment fields)
        invalid = ['Last name', 'First name', 'Email', 'Student ID', 'Total Score']

        for line in reader:

            # LC doesn't always have the student ids stored.
            # Students can choose to use a personal email here, so this isn't a very reliable
            # way of determing the SID. It works for me since, I edited the LC csv gradebook
            # with their official emails in them.
            sid = email_to_sid(line['Email'])

            earned_pts = float(line['Total Score'])
            percent = earned_pts/total_points

            gradebook[sid].add_assignment('lc', min(percent, 1.0) if cap_percent else percent)

def parse_wa(csv_location: str, gradebook: dict, assignment_type: str, total_points: float, points_to_drop: float = 0, cap_percent: bool = True) -> None:
    wa_sid = 'Student ID'
    wa_ptsearned = 'Total'

    with open(csv_location) as csvfile:
        reader = DictReader(csvfile)
        for line in reader:
            sid = email_to_sid(line['Username'])
            percent = float(line[wa_ptsearned].strip())/(total_points-points_to_drop)
            gradebook[sid].add_assignment(assignment_type, min(
                percent, 1.0) if cap_percent else percent)


def write_gradebook(csv_output: str, gradebook: dict, type_weight_dict: dict, type_drops_dict: dict) -> None:
    fieldnames = ['sid', 'lname', 'fname']

    for t in gradebook[list(gradebook.keys())[0]].list_assignment_types():
        fieldnames.append(t)
    fieldnames.append('average')

    with open(csv_output, 'w') as csvout:
        dictwriter = DictWriter(csvout, fieldnames=fieldnames)
        dictwriter.writeheader()
        for sid in gradebook.keys():
            dictwriter.writerow(gradebook[sid].to_summary(
                type_weight_dict, type_drops_dict))
