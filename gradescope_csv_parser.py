import csv
from gradebook import GradeBook

conversion = {
    'student_id' : 'SID',
    'lastname' : 'Last Name',
    'firstname' : 'First Name',
    'total_points' : 'Max Points',
    'score' : 'Total Score'
}

def parse_gradescope_roster(roster_csv_location):
    student_list = []
    with open(roster_csv_location) as roster_csv:
        roster_reader = csv.DictReader(roster_csv)
        for student in roster_reader:
            student_list.append(GradeBook.StudentRow(
                student[conversion['student_id']],
                student[conversion['lastname']],
                student[conversion['firstname']]
            ))

    student_list.sort(key=lambda x: x.to_tuple())
    return student_list

def parse_gradescope_assignment(assignment_csv_location):
    assignment_data = []
    with open(assignment_csv_location) as assignment_csv:
        assignment_reader = csv.DictReader(assignment_csv)

        for row in assignment_reader:
            pts_earned = float(row[conversion['score']]) if row[conversion['score']] != '' else 0.0
            total_pts = float(row[conversion['total_points']])

            assignment_data.append(GradeBook.AssignmentRow(
                row[conversion['student_id']],
                row[conversion['lastname']],
                row[conversion['firstname']],
                total_pts,
                pts_earned,
                pts_earned/total_pts, # The percentage
                None
            ))
    assignment_data.sort(key=lambda x: x.student.to_tuple())
    
    return assignment_data
