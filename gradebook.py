import psycopg2

class PostgresDBLogin:
    def __init__(self, username, password, dbname, postgres_url='localhost', postgres_port='5432'):
        self.password      = password
        self.username      = username
        self.dbname        = dbname
        self.postgres_url  = postgres_url
        self.postgres_port = postgres_port

    @staticmethod
    def env_file_to_login(env_file='.env', url='localhost', port='5432'):
        values = {}
        with open(env_file) as f:
            for line in f:
                pair = line.split(':')
                values[pair[0].strip()] = pair[1].strip()
        return PostgresDBLogin(values['POSTGRES_USER'], values['POSTGRES_PASSWORD'], values['POSTGRES_DB'], postgres_url=url, postgres_port=port)



class GradeBook:
    class GradeBookNotConnectedException(Exception):
        def __init__(self, *args: object) -> None:
            super().__init__(*args)

        def __str__(self) -> str:
            return 'GradeBook is not connected to a Postgres database'
    
    class StudentRow:
        def __init__(self, id, lastname, firstname) -> None:
            self.id = id
            self.lastname = lastname
            self.firstname = firstname
        
        def __str__(self) -> str:
            return '{}, {}, {}'.format(self.lastname, self.firstname, self.id)
        
        def __lt__(self, other):
            if self.lastname < other.lastname:
                return True
            elif self.firstname < other.firstname:
                return True
            else:
                return self.id < other.id
        
        def to_tuple(self):
            return (self.lastname, self.firstname, self.id)

    
    class AssignmentRow:
        def __init__(self, student, total_points, earned_points, percent, question_scores) -> None:
            self.student = student
            self.total_points = total_points
            self.earned_points = earned_points
            self.percent = percent
            self.question_scores = question_scores
        
        def __init__(self, student_id, lastname, firstname, total_points, earned_points, percent, question_scores) -> None:
            self.student = GradeBook.StudentRow(student_id, lastname, firstname)
            self.total_points = total_points
            self.earned_points = earned_points
            self.percent = percent
            self.question_scores = question_scores
        
        def __str__(self):
            return str(self.student)+', Max: {}, Earned: {}, Percent: {}'.format(self.total_points, self.earned_points, self.percent)

    class AssignmentResults:
        # TODO: Create std_dev and variance functions
        def __init__(self, assignment_name, assignment_type, results) -> None:
            self.assignment_name = assignment_name
            self.assignment_type = assignment_type
            self.results = results # TODO: Replace with a deep copy of _results
        
        def __str__(self):
            out = '''Assignment Name: {}
            Assignment Type: {}

            Results:

            '''

            for result in self.results:
                out = out + str(result) + '\n'
            return out

        def get_average(self):
            if len(self.results) > 0:
                return sum([result.percent for result in self.results])/len(self.results)
            else:
                return -1
    
    class StudentReport:
        # TODO: Create __str__
        # TODO: Create average computing function
        def __init__(self, student_id, lastname, firstname, assignment_dict):
            self.student = GradeBook.StudentRow(student_id, lastname, firstname)
            self.assignment_dict = assignment_dict

    exluded_schemata = ['pg_toast','pg_catalog','public','information_schema']

    def __init__(self) -> None:
        self.connection = None
        self.cursor = None

    def connect(self, login_info) -> None:
        self.connection = psycopg2.connect(
            database = login_info.dbname,
            user = login_info.username,
            password = login_info.password,
            host = login_info.postgres_url,
            port = login_info.postgres_port
        )
        self.cursor = self.connection.cursor()

    def commit(self) -> None:
        if self.connection != None:
            self.connection.commit()
        else:
            raise GradeBook.GradeBookNotConnectedException()

    def close(self) -> None:
        if self.connection != None:
            self.connection.close()
            self.connection = None
            self.cursor = None
        else:
            raise GradeBook.GradeBookNotConnectedException()
    
    def create_roster(self)  -> None:
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            self.cursor.execute('''CREATE TABLE roster (
                student_id VARCHAR(50) PRIMARY KEY NOT NULL,
                lastname VARCHAR(50) NOT NULL,
                firstname VARCHAR(50) NOT NULL
                );''')

    def create_roster(self, student_list):
        self.create_roster()
        for student in student_list:
            self.add_student_to_roster(student)
    
    def add_student_to_roster(self, student) -> None:
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            self.cursor.execute("INSERT INTO roster (student_id, lastname, firstname) \
                VALUES ('{}','{}','{}');".format(student.id, student.lastname, student.firstname))
    
    # Returns a list of assignment types. That is returns the list of schemata with the default
    # schemata removed.
    def list_assignment_types(self):
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            self.cursor.execute("SELECT schema_name FROM information_schema.schemata;")

            assignment_types = []
            
            row = self.cursor.fetchone()
            while row is not None:
                asn_type = row[0].strip()
                if asn_type not in GradeBook.exluded_schemata:
                    assignment_types.append(asn_type)
                row = self.cursor.fetchone()

            return assignment_types

    def list_assignments(self, assignment_type):
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            command = "SELECT * FROM pg_catalog.pg_tables WHERE schemaname = '{}';".format(assignment_type)
            self.cursor.execute(command)

            assignments = []
            
            row = self.cursor.fetchone()
            while row is not None:
                assignments.append(row[0].strip())
                row = self.cursor.fetchone()



    # Creates a new Postgres schema for an assignment type.
    # assignment_type - String. The name of the assignment type (schema name)
    def create_assignment_type(self, assignment_type) -> None:
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            self.cursor.execute('CREATE SCHEMA {}'.format(assignment_type))
    
    # Creates a table under the assignment type schema with N question fields.
    # assignment_type - String. The name of the assignment type (=Postgres schema).
    # assignment_name - String. The name of the new assignment (=new Postgres table).
    # number_of_questions - int. The number of questions in the assignment. Is allowed to be 0 if per question
    #                       results aren't wanted.
    def create_assignment(self, assignment_type, assignment_name, number_of_questions) -> None:
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            sql_to_execute='''CREATE TABLE  {}.{} (
                student_id VARCHAR(50) PRIMARY KEY NOT NULL,
                lastname VARCHAR(50) NOT NULL,
                firstname VARCHAR(50) NOT NULL,
                total_points REAL NOT NULL,
                earned_points REAL NOT NULL,
                percentage REAL NOT NULL'''.format(assignment_type, assignment_name)

            for i in range(1, number_of_questions+1):
                sql_to_execute = sql_to_execute + ''',
                q{} REAL'''.format(i)

            sql_to_execute=sql_to_execute+');'
            
            # At this point the command should look like
            # CREATE TABLE <type>.<name> (
            #   student_id VARCHAR(50) PRIMARY KEY NOT NULL,
            #   lastname VARCHAR(50) NOT NULL,
            #   firstname VARCHAR(50) NOT NULL,
            #   q1 REAL,
            #   q2 REAL,
            #   ...
            #   qN REAL
            # );

            self.cursor.execute(sql_to_execute)
    
    def add_row_to_assignment(self, assignment_type, assignment_name, assignment_row) -> None:
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            num_questions = len(assignment_row.question_scores) if assignment_row.question_scores != None else 0
                
            command = "INSERT INTO {}.{} (student_id, lastname, firstname, total_points, earned_points, percentage".format(assignment_type, assignment_name)
            values = "VALUES ('{}', '{}', '{}', {}, {}, {}".format(
                assignment_row.student.id,
                assignment_row.student.lastname,
                assignment_row.student.firstname,
                assignment_row.total_points,
                assignment_row.earned_points,
                assignment_row.percent
            )
            for i in range(1,num_questions+1):
                command = command + ", Q{}".format(i)
                values = values + ", {}".format(assignment_row.question_scores[i-1])
            command = command + ") "
            values = values + ")"

            # At this point the string command + values should be
            # INSERT INTO <type>.<assignment> (student_id, lastname, firstname, earned_points, percentage, q1, q2, ..., qN) \
            #   VALUES (<id>, <lastname>, <firstname>, <score>, <percent>, <q1 score>, <q2 score>, ..., <qN score>)

            self.cursor.execute(command + values)
    
    def add_rows_to_assignment(self, assignment_type, assignment_name, assignment_rows):
        for row in assignment_rows:
            self.add_row_to_assignment(assignment_type,assignment_name,row)

    def get_assignment_summary(self, assignment_type, assignment_name):
        if self.connection == None:
            raise GradeBook.GradeBookNotConnectedException()
        else:
            query = "SELECT * FROM {}.{};".format(assignment_type, assignment_name)
            self.cursor.execute(query)

            results = []

            row = self.cursor.fetchone()

            while row is not None:
                # Rows in assignments created by GradeBook have the form
                # ID LName, FName, total_points, earned_points, percentage, individual question scores list
                results.append(GradeBook.AssignmentRow(row[0],row[1],row[2],row[3],row[4],row[5],None))
            
            return GradeBook.AssignmentResults(assignment_name, assignment_type, results)
